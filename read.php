<?php

// 1 - Reading a file (fread)

$filename = "mon_roman.txt";
$ressource = fopen($filename, "r");

$contents = fread($ressource, filesize($filename));

echo $contents;

fclose($ressource);

// 2 - Reading a file (fle_get_contents)

$filename = "files/mon_roman.txt";
$contents = file_get_contents($filename);

echo $contents;

// 3 - Reading a file line by line
$filename = "files/mon_roman.txt";
$ressource = fopen($filename, "r");

while ($buffer = fgets($ressource)) {
    echo $buffer . " ";
}

fclose($ressource);