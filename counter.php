<?php

$file = "files/counter.txt";

if (!file_exists($file)) {
    $handler = fopen($file, "w");
    fwrite($handler, 1);

} else {
    $count = intval(file_get_contents($file));
    $handler = fopen($file, "w");
 
    fwrite($handler, $count + 1);
}

fclose($handler);
