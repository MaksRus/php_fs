<?php
$dir = "uploads";
$content = "";
$filename = "";
$txtExt = ".txt";

// Creating our "uploads" dir if it doesn't exist
mkdir($dir);

if (isset($_POST['load'])) {
    // Si l'on doit charger un fichier
    $filename = $_POST['load'];
    $file = "$dir/$filename" . $txtExt;

    if (file_exists($file)) {
        $content = file_get_contents($file);
    }

} else if (!empty($_POST['story']) && !empty($_POST['save'])) {
    // Si l'on doit sauvegarder un fichier
    $filename = $_POST['save'];
    $filename = preg_replace("/[^a-zA-Z0-9]/", '', trim($filename));

    if (!empty($filename)) {
        $file = $dir . '/' . $filename . $txtExt;
        
        $content = htmlspecialchars($_POST['story']);
        $handle = fopen($file, "w");
        fwrite($handle, $content);
        fclose($handle);
    }
}

// Récupération des fichiers .txt
$saves = scandir($dir);
//On fait attention à ne récupérer que les fichiers .txt
$saves = array_filter($saves, function($f) {
    return preg_match('/.txt$/', $f);
});
// On ne veut afficher que le nom du fichier SANS l'extension
$saves = array_map(function($f) {
    return preg_replace('/.txt$/', "", $f);
}, $saves);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
            background-color: #333;
            color: white;
        }

        input,
        textarea {
            background-color: #444;
            color: white;
        }
    </style>
</head>

<body>
    <div class="my-text-editor">

        <?php if (!empty($saves)) : ?>
            <form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST">
                <select name="load" id="load">
                    <?php foreach ($saves as $save) : ?>
                        <option value="<?= $save ?>" <?= $filename === $save ? "selected" : "" ?>> <?= $save ?></option>
                    <?php endforeach ?>
                </select>
                <button type="submit">Load</button>
            </form>
        <?php endif ?>

        <form action="<?=$_SERVER['PHP_SELF'] ?>" method="post">
            <textarea name="story" id="story" cols="60" rows="30"><?= $content ?></textarea>
            <div>
                <input type="text" required name="save" value=<?= $filename ?>>
                <button type="submit">Save</button>
            </div>
        </form>
    </div>
</body>

</html>
