# PHP File System

---
## Lecture / Ecriture
- Créez depuis votre IDE un fichier .txt avec un peu de contenu
- Ecrivez un script PHP qui vous affiche le contenu de ce fichier .txt
- Adaptez votre script pour qu'il modifie le contenu du fichier
--- 

---
## Compteur
- A chaque fois que la page counter.php est visitée on incrémente un Compteur
- Ce compteur doit être un fichier dédié qui persistera pour tous les utilisateurs
- Cet exercice vous demandera de manipuler la lecture ET l'écriture
---

---
## CSV --> HTML
- Votre but est de restituer les informations contenues dans un fichier .csv
- Le fichier employees.csv vous est fourni
- Les informations devront être restituées au sein d'une page HTML sous forme d'élément ```<table>```
- Le nom et le nombre de colonnes se trouvent à la première ligne du fichier employees.csv
---

## Editeur de texte
Maintenant on créé un éditeur de texte ! Rien que ça ! 
Procédez étape par étape : cela va en se complexifiant.

- Avoir un champ sur la page permettant de saisir du texte
- Avoir un bouton "sauvegarder" qui enregistre notre texte sut le serveur
- Pouvoir en plus récupérer son travail après rechargement de la page
- Pouvoir choisir le nom du fichier dans lequel sera enregistré notre texte
- Pouvoir demander à récupérer un fichier précis pour l'éditer dans le navigateur
- Devoir s'authentifier pour accéder à SES fichiers (et donc rendre impossible l'accès aux fichiers des autres utilisateurs)



